package com.test.subject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.subject.entity.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Long> {

}
