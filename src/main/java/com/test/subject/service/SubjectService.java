package com.test.subject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.subject.entity.Subject;
import com.test.subject.repository.SubjectRepository;

@Service
public class SubjectService {

	@Autowired
	private SubjectRepository subjectRepository;

	public List<Subject> getAllSubject() {
		return subjectRepository.findAll();
	}

	public Subject getSubjectById(Long id) {
		return subjectRepository.findById(id).orElse(null);
	}

	public Subject saveSubject(Subject subject) {
		return subjectRepository.save(subject);
	}

	public void deleteSubject(Long id) {
		subjectRepository.deleteById(id);
	}

}
