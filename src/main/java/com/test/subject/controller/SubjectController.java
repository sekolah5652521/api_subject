package com.test.subject.controller;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.subject.entity.Subject;
import com.test.subject.service.SubjectService;

@RestController
@RequestMapping("/api/subject")
public class SubjectController {

	@Autowired
	private SubjectService subjectService;

	private Logger log = LogManager.getLogger(SubjectController.class);

	@GetMapping
	public List<Subject> getAllSubject() {
		return subjectService.getAllSubject();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Subject> getStudentById(@PathVariable Long id) {
		return ResponseEntity.ok(subjectService.getSubjectById(id));
	}

	@PostMapping
	public ResponseEntity<Subject> createStudent(@Valid @RequestBody Subject subject) throws ParseException {

		subjectService.saveSubject(subject);

		return new ResponseEntity<>(subject, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteSubject(@PathVariable Long id) {
		log.info("=============== DELETE SUBJECT ===============");

		subjectService.deleteSubject(id);

		log.info("Done delete subject, id : " + id);
		log.info("=============== END DELETE SUBJECT ===============");

		return ResponseEntity.noContent().build();
	}
}
